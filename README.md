# CSS 341 - Machine Problem #5  #

## Problem Solving with Data Base, text files and spreadsheets ##

	

We have become the “GoTo” team for the Keystone Daily’s general IT problems.  This is highly favorable, since we will now be able to avoid competitive bidding on some urgent projects like this one.

Keystone Daily has a database problem for us to solve for their Human Resources department.  It turns out that last summer they had an IT intern design a personnel data base using Access.  This was considered a major success largely because the intern was the CIO’s nephew and even better, he worked for free!  The one drawback of this project is that the intern departed before populating the database with the necessary personnel data to be collected from a number of sources.  It is to us that the CIO turns in an attempt to finish this project before the end of the calendar year..

There will be many employee records to add to this database, as the Daily has a couple thousand employees in all.  The CIO sees our experience working with automation programming of Office products as suiting her needs well.

Working at the intern’s direction last summer, the HR clerks collected much of the data in machine readable formats.  There are a number of Excel spreadsheets containing up to approximately 100 employees that have been generated.  There are also a number of plain text files with information about employee health plans, banks, retirement plans, newspaper departments, etc.  The Excel sheets are to be included automatically into the main data table and the text files are to be used to update the other data tables. 

At this point, all but one of the employee spreadsheets seem to be unavailable.  We suspect that the intern misplaced them.  So, the CIO wants us to develop a program that will update the appropriate tables with the text files and update the master data table with the spreadsheet information.  The IT staff will then be able to run the program and further augment the database as more of the input files are located.

She also wants us to demonstrate three queries to show that the database is working.  These queries will be the final report on this part of the project.  We can train the staff in HR to run the program on the remaining excel personnel sheets when they are found.


We also know that the HR Dept will in the future need a smooth data-entry interface and report generator if this database project is successful.  We are hopeful that success here will bring a contract for that as well.

*The present situation can be summarized as follows:
*## Information Available ##
Access data base file named KeystoneHR.accdb, which contains the data tables: Buildings; DivisionInfo; EmployeeBanks; MedicalPlans; Personnel Info; and RetirementPlans and three queries. This database is populated with the information needed for just two employees.
Excel spreadsheet PersonnelI.xls (or xlsx) containing approximately one-hundered personnel records.  Each employee record is found on a spreadsheet row with fifteen fields (columns) of information.
Text files named BanksNew.txt; BldgsNew.txt; DivisionsNew.txt; HealthPlansNew.txt; and RetirementPlansNew.txt.  These files contain the information needed to bring their respective data tables up to date for the set of employees being added in this spreadsheet.  The data records (one per line) contain the necessary information in fields that are separated by “;” (semicolon).
The headings on the Access fields and those on the Excel columns have obvious correlation with each other.  Those fields in the text files are in the same order as the fields in their respective Access tables.
## Product to be produced ##
Access database containing information for all of the employees and all other tables updated as needed for this set of employees.  This includes the addition of the new employee records to the existing ones.
JavaScript program, with high quality html web page interface that is used to update the database tables as more employee information is found and conduct the test queries.
Ouput from queries intended to test that you have properly updated the data base. These three queries against the database should report::
All employees in alphabetical order, with dates of employment, dates of birth, and medical plans;
All employees (alphabetical order)  in the accounting division, with work location, mailstop, and  manager;  and
An alphabetical list in descending order of all female employees resident in any particular state (entered as a parameter).  (For example, all female employees from WA.)
These test queries should be chosen by the user from the web page interface.


## In more detail here is what the HR director wants from our system:
 ##
When our system starts, it should ask the user to select an Access file and make a working
copy of it, so as to protect the original data from loss.  Your program should allow selection of  both *.mdb or *.accdb Access programs since both older and newer vesions of Office are in use at the Daily. See CopyDataBaseFile.htm from Week 8 demos.
The system should ask the user to identify one by one, the (up to five) data tables in the Access file to be updated and to select the *.txt files providing update information for each:
Perhaps check boxes indicating which tables are to be updated would prompt for selection of each appropriate update text file.
It is fairly obvious how the character strings from the text file lines map into the table fields.
There is a column of codes in the Bank, Health Plans, and Retirement data tables.  For example, the bank Citizens Saving and Loan has the code CSL.  The code column must be filled with exact matches to the codes found in the text files.  This is necessary because the personnel info table has been designed to allow only exact matches to the short codes in the associated tables.
This data will be fairly easy to parse from the text files, since the fields are separated by semicolons and should not be highly corrupted, since they were typed pretty carefully.  However, the data has not been validated.
Actual addition of the new records to each table can be done with a procedure similar to the function AddNewRowToRecordSetWithFieldsArray() found in the demo AdoAccess of Week 9.  Instead of filling the Data array from user prompt input, you would fill it from the strings derived from the text files.

The system would then ask the user for an Excel spreadsheet to update the Personnel Info data table, assuming the user had indicated this, perhaps with a check box.
Each row of the spreadsheet will correspond to one new row added to the data table Personnel Info.  There are about 15 fields, some of which require data manipulation before being added.
The name field of the spreadsheet must be parsed to provide last, first, and middle names for the data base.  If no middle name or initial is present, it should be left blank.
Street addresses are largely ok as found.  But, those with apartment units in the form “Ap xxx-street number”  or “xxx-street number”  should be changed so that the apartment number follows in the form “street number, Apt xxx”.
The city name in the spreadsheet seem to be garbage.  Actually, the intern tried to use a AJAX lookup function to fill in city names from zip codes entered in the spreadsheet.  He did not use this correctly and provided random city names, completely unassociated with the entered zip code.  Your program needs to provide correct city names for the zip codes.  See below for how to do this.
The state name is missing from the Excel sheet and must be derived from the zip code provided as well..  One source of this information is:
https://code.google.com/p/ziplookup/

From this you can find a state symbol, state name, and city name for any valid zip code in the call back function that you provide.  As an implementation example of this
routine, see the demo program from Week9: ZipTest.html.  Your program would call this table function with a zip code as the argument, and the function should in turn call your callback function and  return the desired information.

There are two important issues:
The zip code data in the spreadsheet has been typed carefully, but has not been validated.  Therefore, there are likely to be a few zip code entries that are not currently valid zip codes.  You need to handle this in a way that makes sense in the context of this program.  It would not be suitable, for example, to halt execution with an alert box asking the user for a valid zip code.  The user has no way of providing that information.
To repeat, the city names need to be replaced at this step.
Phone numbers will need to be formatted as xxx-yyy-zzzz
Dates of birth and employment need to be formatted as: mm/dd/yyyy
If the spreadsheet indicates two retirement plans, enter one in each of the two Access fields.  Most workers will have only one plan or none at all.

The three queries described above have been incorporated into the database template. 
A good approach would be to have the desired query selected from a menu input and then have a button to fire off the selected query.  Your program will need to run them automatically and display the results. 
Note that one of the queries is a parameter (user provided) search. 
The  PrintRecordSet() function of AdoAcess (Week 8 demo) gives a framework for displaying results.  Instead of displaying the messages as alert boxes, you would want to display them as text areas of the web page on the first version.
On the final version  you should put the query printouts on *.txt or *.docx files.  Text area will be easiest and needs to be there.   You should set that up initially and then try to add a nicer hard copy as a user selected option toward the end of the project.